package com.hva.daan;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Main {
    private static final Sentient trump = new Trump();

    public static void main(String[] args) {
        List<Sentient> allDomain = prepareForAllDomain();
        List<Sentient> existDomain = prepareThereExistDomain();
        Predicate<Sentient> predicate = preparePredicate();
        boolean resultForAll = forAll(allDomain, predicate);
        boolean resultForAllShouldBeFalse = forAll(existDomain, predicate);
        boolean resultThereExists = thereExists(existDomain, predicate);
        System.out.println("True: " + resultForAll);
        System.out.println("False: " + resultForAllShouldBeFalse);
        System.out.println("True: " + resultThereExists);
    }

    private static List<Sentient> prepareForAllDomain() {
        List<Sentient> persons = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Sentient person = new Person();
            person.setLikes(trump);
            persons.add(person);
        }

        return persons;
    }

    private static List<Sentient> prepareThereExistDomain() {
        List<Sentient> persons = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Sentient person = new Person();
            if (i % 2 == 0) {
                person.setLikes(trump);
            }

            persons.add(person);
        }

        return persons;
    }

    private static Predicate<Sentient> preparePredicate() {
        return sentient -> sentient.likes(trump);
    }

    private static boolean forAll(List<Sentient> sentients, Predicate<Sentient> predicate) {
        return sentients
                .stream()
                .allMatch(predicate);
    }

    private static boolean thereExists(List<Sentient> sentients, Predicate<Sentient> predicate) {
        return sentients
                .stream()
                .anyMatch(predicate);
    }
}
