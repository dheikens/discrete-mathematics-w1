package com.hva.daan;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Person implements Sentient {
    private final String identity;
    private List<Sentient> sentients = new ArrayList<>();

    public Person() {
        this.identity = UUID.randomUUID().toString();
    }

    @Override
    public boolean likes(Sentient other) {
        return this.sentients.contains(other);
    }

    @Override
    public void setLikes(Sentient other) {
        this.sentients.add(other);
    }

    @Override
    public String getIdentity() {
        return this.identity;
    }
}
