package com.hva.daan;

import java.util.ArrayList;
import java.util.List;

public class Trump implements Sentient {
    private List<Sentient> sentients = new ArrayList<>();

    @Override
    public boolean likes(Sentient other) {
        return this.sentients.contains(other);
    }

    @Override
    public void setLikes(Sentient other) {
        this.sentients.add(other);
    }

    @Override
    public String getIdentity() {
        return "trump";
    }
}
